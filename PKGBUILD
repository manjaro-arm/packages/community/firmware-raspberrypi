# Maintainer: Ray Sherwin <sliick517d@gmail.com>
# Contributor: Dan Johansen <strit@manjaro.org>
# Arch Linux ARM Contributor: graysky <therealgraysky AT protonmail DOT com>
# Arch Linux ARM Contributor: Kevin Mihelich <kevin@archlinuxarm.org>

pkgname=firmware-raspberrypi
_firmcommit=4b356e134e8333d073bd3802d767a825adec3807
#_bluezcommit=d9d4741caba7314d6500f588b1eaa5ab387a4ff5
pkgver=20240810
pkgrel=1
pkgdesc="Additional firmware for Raspberry Pi"
arch=('any')
url="https://github.com/RPi-Distro/firmware-nonfree"
license=('custom')
makedepends=('git')
options=('!strip')
source=("git+https://github.com/RPi-Distro/firmware-nonfree.git#commit=$_firmcommit"
        # support for RPi400, RPi4-compute-module
        "BCM4345C5.hcd::https://raw.githubusercontent.com/RPi-Distro/bluez-firmware/bookworm/debian/firmware/broadcom/BCM4345C5.hcd"
        # bluetooth for RPi3+
        "BCM43430A1.hcd::https://raw.githubusercontent.com/RPi-Distro/bluez-firmware/bookworm/debian/firmware/broadcom/BCM43430A1.hcd"
        "BCM4345C0.hcd::https://raw.githubusercontent.com/RPi-Distro/bluez-firmware/bookworm/debian/firmware/broadcom/BCM4345C0.hcd"
        # bluetooth for Zero 2 W
        "BCM43430B0.hcd::https://raw.githubusercontent.com/RPi-Distro/bluez-firmware/bookworm/debian/firmware/broadcom/BCM43430B0.hcd"
        'RPi-WM8804.conf')

sha256sums=('247bdaa9d3596fabb84f050b5e14cd990e224e9151f77528caec23fab7bcdaaa'
            'fb9f4ec2df5301bd35f416384e103c012c5983024c49aa72fbbaf90177512caa'
            'c096ad4a5c3f06ed7d69eba246bf89ada9acba64a5b6f51b1e9c12f99bb1e1a7'
            '51c45e77ddad91a19e96dc8fb75295b2087c279940df2634b23baf71b6dea42c'
            '338c2c6631131f516bfc7e64ef0872bd0402e1f98ef9d0c900eef0c814d90a25'
            'f978fbc40db75ba3213a4472023496d0716706eb1a6f078f207ac027c5753f43')

package() {
  _FWPATH=firmware-nonfree/debian/config/brcm80211

  # https://github.com/RPi-Distro/firmware-nonfree/issues/26
  unlink "$srcdir/$_FWPATH/brcm/brcmfmac43455-sdio.bin"

  # install cypress dir and symlink cyfmac43455-sdio-standard.bin which debian
  # packaging does, see README.txt in /cypress and comments in this commit:
  # https://github.com/RPi-Distro/firmware-nonfree/commit/3b108c864428dda9f9833cf3346f438552d08cea
  install -d "$srcdir/$_FWPATH/cypress" "$pkgdir"/usr/lib/firmware/updates/cypress/
  install -m 0644 "$srcdir/$_FWPATH/cypress/"* "$pkgdir"/usr/lib/firmware/updates/cypress
  cd "$pkgdir"/usr/lib/firmware/updates/cypress

  # we can use either the -standard or -minimal files so use standard by default
  ln -s cyfmac43455-sdio-standard.bin cyfmac43455-sdio.bin

  cd "$srcdir"
  install -d "$pkgdir"/usr/lib/firmware/updates/brcm "$pkgdir"/usr/share/alsa/cards/
  install -m 0644 *.hcd "$pkgdir"/usr/lib/firmware/updates/brcm
  install -m 0644 RPi-WM8804.conf "$pkgdir"/usr/share/alsa/cards/
  cp -r "$srcdir/$_FWPATH/brcm/"* "$pkgdir"/usr/lib/firmware/updates/brcm

  # make some symbolic links
  cd "$pkgdir"/usr/lib/firmware/updates/brcm
  ln -s brcmfmac43456-sdio.txt brcmfmac43456-sdio.pine64-pinephone-pro.txt
  # This now exists
#  ln -s brcmfmac43455-sdio.raspberrypi,4-model-b.txt brcmfmac43455-sdio.raspberrypi,4-compute-module.txt
  # They changed the offical pi5 model name in the kernel
  # https://github.com/raspberrypi/linux/commit/b6bfece0d9ddf21e1526fead81340ef02f98f6ad
  ln -s BCM43430B0.hcd BCM.hcd
  ln -s BCM4345C5.hcd BCM4345C5.raspberrypi,400.hcd
  ln -s BCM4345C5.hcd BCM4345C5.raspberrypi,4-compute-module.hcd
  ln -s BCM43430A1.hcd BCM43430A1.raspberrypi,3-model-b.hcd
  ln -s BCM43430A1.hcd BCM43430A1.raspberrypi,model-zero-2-w.hcd
  ln -s BCM4345C0.hcd BCM4345C0.raspberrypi,3-model-a-plus.hcd
  ln -s BCM4345C0.hcd BCM4345C0.raspberrypi,3-model-b-plus.hcd
  ln -s BCM4345C0.hcd BCM4345C0.raspberrypi,4-compute-module.hcd
  ln -s BCM4345C0.hcd BCM4345C0.raspberrypi,4-model-b.hcd
  ln -s BCM4345C0.hcd BCM4345C0.raspberrypi,5-model-b.hcd
}
